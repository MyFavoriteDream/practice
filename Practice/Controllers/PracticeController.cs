﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.RegularExpressions;
using static Practice.StringOperations;
using static System.Net.WebRequestMethods;

namespace Practice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PracticeController : Controller
    {


        [HttpGet(Name = "GetResult")]
        public async Task<IActionResult> Get(string input, int sortSelection)
        {

            string output = string.Empty;
            string vowelString = string.Empty;
            Dictionary<char, int> stats = new Dictionary<char, int>();
            string sortString = string.Empty;
            string stringSymbolDelete = string.Empty;

            string test = StringOperations.StringSearch.Search(input);

            string jsonText = String.Empty;
            using (var reader = new System.IO.StreamReader(Directory.GetCurrentDirectory() + "\\appsettings.json"))
            {
                jsonText = reader.ReadToEnd();
            }

            Appsettings settings = new Appsettings();
            settings = JsonSerializer.Deserialize<Appsettings>(jsonText);
            

            if (StringSearch.SearchInBlackList(input, settings) != -1)
            {
                return BadRequest("HTTP ошибка 400 Bad Request. " + "Слово находится в черном списке.");
            }
            else if (test == "")
            {
                if (input.Length % 2 == 0)
                    output = StringOperations.StringChange.IfEven(input);
                else
                    output = StringOperations.StringChange.IfOdd(input);

                test = "В строке нет неподходящих символов";

                stats = StringOperations.StringCount.SymbolCount(output);
                vowelString = StringOperations.StringSearch.SearchSubString(output);
                sortString = StringOperations.StringSort.SortSelection(output, sortSelection);
                stringSymbolDelete = await StringOperations.StringSymbolDelete.SymbolDelete(output, settings.RandomApi);

                return Ok (new Result
                {
                    //Исходная строка
                    InputString = "Исходная строка: " + input,
                    //Проверка строки
                    StringValidation = test,
                    //Обработанная строка
                    ProcessedString = "Обработанная строка: " + output,
                    //Наибольшая строка, начинающаяся и заканчивающаяся на гласную
                    StartsAndEndsWithAVowelString = "Наибольшая строка, начинающаяся и заканчивающаяся на гласную: " + vowelString,
                    //Статистика обработанной строки
                    StringStatistics = stats,
                    //Отсортированная строка
                    SortedString = "Отсортированная строка: " + sortString,
                    //Строка без одного символа
                    StringSymbolDelete = "Строка без одного символа: " + stringSymbolDelete
                });
            }
            else
            {
                return BadRequest("HTTP ошибка 400 Bad Request. " + test);
            }       
        }
    }

}
