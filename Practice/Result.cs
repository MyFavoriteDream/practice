﻿namespace Practice
{
    public class Result
    {
        public string? InputString { get; set; }

        public string? StringValidation { get; set; }

        public string? ProcessedString { get; set; }

        public string? StartsAndEndsWithAVowelString { get; set; }

        public Dictionary<char, int> StringStatistics { get; set; }

        public string? SortedString { get; set; }

        public string? StringSymbolDelete { get; set; }

    }
}
