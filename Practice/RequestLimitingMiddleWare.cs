﻿namespace Practice
{
    public class RequestLimitingMiddleware
    {

        private readonly RequestDelegate _next;
        private SemaphoreSlim _semaphore;
        private readonly int _maxConcurrentRequests;

        public RequestLimitingMiddleware(RequestDelegate next, IConfiguration config)
        {
            _next = next;
            _maxConcurrentRequests = config.GetValue<int>("Settings:ParallelLimit");
            _semaphore = new SemaphoreSlim(_maxConcurrentRequests);
        }

        public async Task Invoke(HttpContext context)
        {
            if (!_semaphore.Wait(0))
            {
                context.Response.StatusCode = 503;
                context.Response.ContentType = "text/html; charset=utf-8";
                await context.Response.WriteAsync("HTTP ошибка 503 Service Unavailable");

                return;
            }

            try
            {
                await _next(context);
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}
