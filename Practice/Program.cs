using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Practice;
using System.Net;
//using Microsoft.AspNetCore.RateLimiting;
using System.Threading.RateLimiting;
//using WebRateLimitAuth.Models;
using Microsoft.AspNetCore.ConcurrencyLimiter;
using AspNetCoreRateLimit;

var builder = WebApplication.CreateBuilder(args);

var confBuilder = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

IConfiguration config = confBuilder.Build();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<RequestLimitingMiddleware>(config);


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();