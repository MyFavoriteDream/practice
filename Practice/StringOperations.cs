﻿using System.Net.Mime;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Text;

namespace Practice
{
    public class StringOperations
    {

        //Задание 6
        public static class StringSymbolDelete
        {
            static readonly HttpClient client = new HttpClient();

            //Создания json-Строки
            public static string JsonStringCreate(int stringLength)
            {
                var w = new Parameters
                {
                    apiKey = "104b4010-0154-44a1-a283-eb1962e47cd4",
                    n = 1,
                    min = 0,
                    max = stringLength - 1,
                    replacement = true,
                    _base = 10,
                    pregeneratedRandomization = null
                };
                var q = new RequestBuilder
                {
                    jsonrpc = "2.0",
                    method = "generateIntegers",
                    id = 11678,
                    @params = w
                };

                string jsonString = JsonSerializer.Serialize(q);
                string tmp = jsonString.Substring(0, jsonString.IndexOf("_base")) + jsonString.Substring(jsonString.IndexOf("_base") + 1);

                return tmp;
            }
            //Генерация случайного числа для удаления символа
            public static async Task<int> DeleteId(int stringLength, string url)
            {
                int idNumber = 0;
                //Random.org
                //var url = "https://api.random.org/json-rpc/4/invoke";

                string jsonString = JsonStringCreate(stringLength);

                try
                {
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Post,
                        RequestUri = new Uri(url),
                        Content = new StringContent(jsonString, Encoding.UTF8, MediaTypeNames.Application.Json),
                    };

                    var response = await client.SendAsync(request);
                    response.EnsureSuccessStatusCode();

                    var responseBody = await response.Content.ReadAsStringAsync();

                    //Если ошибка. Например, недоступен ключ
                    if (responseBody.Contains("error") == false)
                    {
                        string tmp = responseBody.Substring(responseBody.IndexOf("[") + 1, responseBody.IndexOf("]") - responseBody.IndexOf("[") - 1);
                        idNumber = Convert.ToInt32(tmp);
                    }
                    else
                    {
                        Random random = new Random();
                        idNumber = random.Next(0, stringLength - 1);
                    }

                    //Console.WriteLine(responseBody);
                }
                catch (HttpRequestException e)
                {
                    Random random = new Random();
                    idNumber = random.Next(0, stringLength - 1);

                    //Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", e.Message);
                }

                return idNumber;
            }

            //Удаление символа
            public static async Task<string> SymbolDelete(string input, string url)
            {
                int idNumber = await DeleteId(input.Length, url);

                return input.Substring(0, idNumber) + input.Substring(idNumber + 1);
            }

        }
        //Задание 5
        public static class StringSort
        {
            public static string SortSelection(string input, int choice)
            {
                //string result = string.Empty;

                if (choice == 1)
                {
                    return "Выбрана быстрая сортировка. Отсортированная строка: " + StringSort.QuickSort.Sort(input.ToCharArray(), 0, input.Length - 1);

                }
                else if (choice == 2)
                {
                    return "Выбрана сортировка деревом. Отсортированная строка: " + StringSort.TreeSort.Sort(input.ToCharArray());

                }
                else if (choice == 0)
                {
                    return "Выбрано не сортировать строку.";
                }
                else
                {
                    return "Введенное значение некорректно.";
                }
            }
            // Быстрая сортировка
            public class QuickSort
            {
                // Перестановка двух элементов
                static void Swap(ref char first, ref char second)
                {
                    var swap = first;
                    first = second;
                    second = swap;
                }

                //Выбор опорного элемента
                static int Partition(char[] array, int left, int right)
                {
                    var pivot = left - 1;
                    for (var i = left; i < right; i++)
                    {
                        if (array[i] < array[right])
                        {
                            pivot++;
                            Swap(ref array[pivot], ref array[i]);
                        }
                    }

                    pivot++;
                    Swap(ref array[pivot], ref array[right]);
                    return pivot;
                }

                //Cортировка
                public static string Sort(char[] array, int left, int right)
                {
                    string result = string.Empty;

                    if (left >= right)
                    {
                        for (int i = 0; i < array.Length; i++)
                            result += array[i];

                        return result;
                    }

                    var pivotIndex = Partition(array, left, right);
                    Sort(array, left, pivotIndex - 1);
                    Sort(array, pivotIndex + 1, right);

                    for (int i = 0; i < array.Length; i++)
                        result += array[i];

                    return result;
                }

            }
            //Сортировка деревом
            public class TreeSort
            {
                static public int max = 0;
                public class tree
                {
                    public char value;
                    public tree left;
                    public tree right;
                }

                static public tree add_to_tree(tree root, char new_value)
                {

                    if (root == null)
                    {
                        root = new tree();
                        root.value = new_value;
                        root.left = null;
                        root.right = null;
                        return root;
                    }

                    if (root.value < new_value)
                        root.right = add_to_tree(root.right, new_value);
                    else
                        root.left = add_to_tree(root.left, new_value);

                    return root;
                }

                static public void tree_to_array(tree root, char[] arr)
                {
                    if (root == null) return;
                    tree_to_array(root.left, arr);

                    arr[max++] = root.value;
                    tree_to_array(root.right, arr);
                }

                //Сортировка
                static public string Sort(char[] arr)
                {
                    string result = string.Empty;

                    tree root = null;

                    for (int i = 0; i < arr.Length; i++)
                    {
                        root = add_to_tree(root, arr[i]);
                    }

                    tree_to_array(root, arr);

                    for (int i = 0; i < arr.Length; i++)
                        result += arr[i];

                    return result;
                }
            }

        }

        //Задание 3
        public static class StringCount
        {
            // Подсчет статистики
            public static Dictionary<char, int> SymbolCount(string input)
            {
                Dictionary<char, int> Stats = new Dictionary<char, int>();

                for (int i = 0; i < input.Length; i++)
                {
                    if (Stats.ContainsKey(input[i]))
                        Stats[input[i]]++;
                    else
                        Stats.Add(input[i], 1);
                }

                return Stats;
            }

        }

        //Задание 2
        public static class StringSearch
        {
            //Поиск слова в черном списке
            public static int SearchInBlackList(string input, Appsettings settings)
            {
                return settings.Settings.BlackList.IndexOf(input);
            }
            public static string Search(string input)
            {
                Regex rx = new Regex(@"[^a-z]");

                string result = string.Empty;
                MatchCollection matches = rx.Matches(input);
                string[] arr = matches.Cast<Match>().Select(m => m.Value).ToArray();

                if (matches.Count > 0)
                {

                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (result.IndexOf(arr[i]) == -1)
                            result += "-" + arr[i];
                    }

                    result = "Введены неподходящие символы: " + result;
                }

                return result;
            }

            // Задание 4
            public static string SearchSubString(string input)
            {
                //Словарь гласных
                var Vowels = new Dictionary<int, char>()
                {
                    [1] = 'a',
                    [2] = 'e',
                    [3] = 'i',
                    [4] = 'o',
                    [5] = 'u',
                    [6] = 'y'
                };

                string output = string.Empty;
                int begin = -1;
                int end = input.Length;
                bool j = false;

                // Поиск первой гласной
                while (j == false && begin < input.Length - 1)
                {
                    begin++;

                    if (Vowels.ContainsValue(input[begin]))
                        j = true;
                }
                // Если гласных в строке не оказалось
                if (begin == (input.Length - 1) && j == false)
                {
                    output = "В строке отсутствуют гласные";
                }
                else
                {
                    // Поиск последней гласной
                    while (j == true && end >= begin)
                    {
                        end--;

                        if (Vowels.ContainsValue(input[end]))
                            j = false;
                    }

                    output = input.Substring(begin, end - begin + 1);
                }

                return output;
            }
        }

        //Задание 1
        public static class StringChange
        {
            //Переворот строки
            static string Reverse(string input)
            {
                char[] arr = input.ToCharArray();
                Array.Reverse(arr);
                return new string(arr);
            }

            //Если число четное
            public static string IfEven(string input)
            {

                string tmp1 = input.Substring(0, input.Length / 2);
                string tmp2 = input.Substring(input.Length / 2);

                string output = Reverse(tmp1) + Reverse(tmp2);

                return output;
            }

            //Если число нечетное
            public static string IfOdd(string input)
            {
                string output = Reverse(input) + input;

                return output;
            }
        }

    }
}
