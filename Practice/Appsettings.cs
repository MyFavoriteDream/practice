﻿namespace Practice
{
    public class Appsettings
    {
        
        public string? RandomApi { get; set; }
        
        public Settings Settings { get; set; }
    }

    public class Settings
    {
        
        public List<string> BlackList { get; set; }

        public int ParallelLimit { get; set; }

        //public int NumberOfCurrentRequests { get; set; }
    }
}
