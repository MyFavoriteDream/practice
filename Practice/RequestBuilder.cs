﻿namespace Practice
{
    public class RequestBuilder
    {
        public string? jsonrpc { get; set; }
        public string? method { get; set; }

        public Parameters @params { get; set; }

        public int id { get; set; }
    }

    public class Parameters
    {
        public string? apiKey { get; set; }

        public int n { get; set; }

        public int min { get; set; }

        public int max { get; set; }

        public bool replacement { get; set; }

        public int _base { get; set; }

        public string pregeneratedRandomization { get; set; }
    }
}
